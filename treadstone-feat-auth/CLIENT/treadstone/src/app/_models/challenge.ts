export interface challenge{
    id:number;
    title:string;
    descripcion:string;
    lenguajes:string[];
    puntos:number;
    intentos:number;
    dificultad:string;
    empresa:string;
}