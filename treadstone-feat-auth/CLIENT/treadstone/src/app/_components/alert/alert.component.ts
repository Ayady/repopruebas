import { Component, OnInit, OnDestroy  } from '@angular/core';
import { Subscription } from 'rxjs';

import { AlertService } from "../../_services/alert.service";

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {
  private subscription: Subscription;
  message: any;

  //falta inicializar en el construc el alert service
  constructor(private alertService: AlertService) { }

  ngOnInit() {

    this.subscription = this.alertService.GetMessage().subscribe(message => { 
      this.message = message; 
      });

  }


  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
