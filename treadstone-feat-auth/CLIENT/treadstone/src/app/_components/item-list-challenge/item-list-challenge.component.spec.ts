import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListChallengeComponent } from './item-list-challenge.component';

describe('ItemListChallengeComponent', () => {
  let component: ItemListChallengeComponent;
  let fixture: ComponentFixture<ItemListChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemListChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
