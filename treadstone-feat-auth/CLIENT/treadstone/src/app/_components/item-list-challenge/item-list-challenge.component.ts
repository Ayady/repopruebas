import { Component, OnInit } from '@angular/core';
import {ChallengesService} from './../../_services/challenges.service'
import {challenge } from "./../../_models/challenge"

@Component({
  selector: 'app-item-list-challenge',
  templateUrl: './item-list-challenge.component.html',
  styleUrls: ['./item-list-challenge.component.css']
})
export class ItemListChallengeComponent implements OnInit {

  // title:string;
  // descripcion:string;
  // lenguajes:string[];
  // puntos:number;
  // intentos:number;
  // dificultad:string;
  // empresa:string;
  
  challenges:challenge ={
    id:1,
    title:"Say Hello Word",
    descripcion:"Say Hello Word :)",
    lenguajes:["C#","Java","Go","C++"],
    puntos:666,
    intentos:6,
    dificultad:"Hard",
    empresa:"Google"
  };
   
  challengess:any[];

  constructor(private challengesService:ChallengesService) {
  
  }

  ngOnInit() {
    this.challengesService.getChallenge().subscribe(
      res=>(res),
      err=> console.log(err)
    )

  }

}
