import { Component, OnInit } from '@angular/core';
import {ChallengesService} from './../../_services/challenges.service'
import {challenge } from "./../../_models/challenge"

@Component({
  selector: 'app-lis-challenge',
  templateUrl: './lis-challenge.component.html',
  styleUrls: ['./lis-challenge.component.css']
})
export class LisChallengeComponent implements OnInit {

    // challenges:challenge={
  //   id:1,
  //   title:"Say Hello Word",
  //   descripcion:"Say Hello Word :)",
  //   lenguajes:["C#","Java","Go","C++"],
  //   puntos:666,
  //   intentos:6,
  //   dificultad:"Hard",
  //   empresa:"Google"
  // };

  constructor(private challengesService:ChallengesService) { 
  }

  ngOnInit() {
  }

}
