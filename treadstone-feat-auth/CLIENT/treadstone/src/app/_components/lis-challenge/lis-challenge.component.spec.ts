import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LisChallengeComponent } from './lis-challenge.component';

describe('LisChallengeComponent', () => {
  let component: LisChallengeComponent;
  let fixture: ComponentFixture<LisChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LisChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LisChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
