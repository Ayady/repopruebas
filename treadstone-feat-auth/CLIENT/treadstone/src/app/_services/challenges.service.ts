import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import {challenge} from "./../_models/challenge"

@Injectable({
  providedIn: 'root'
})
export class ChallengesService {
  api='http://localhost:3000';

  constructor(private http:HttpClient) { }

  getChallenges(id:any){
    return this.http.get(`${this.api}/challenges?id=${id}`);
  }

  getChallenge(){
    return this.http.get(`${this.api}/challenges}`);
  }



}

